/*
 * Copyright (c) 2018 Mastiff
 *
 * This file is licensed under the MIT license found in the LICENSE file in the
 * root directory of this source tree.
 *
 */

module.exports = {
  testURL: 'http://localhost/',
  collectCoverageFrom: [
    'src/**/*.{js,jsx}',
    '!src/**/*.test.{js,jsx}'
  ],
  coverageThreshold: {
    global: {
      statements: 75,
      branches: 60,
      functions: 65,
      lines: 75
    },
  },
  setupFiles: [
    '<rootDir>/testing/setupJest.js'
  ],
  testRegex: '__tests__/.*\\.test\\.jsx?$'
};
