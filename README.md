# Mock Firestore

[![pipeline status](https://gitlab.com/mastiff/mock-firestore/badges/master/pipeline.svg)](https://gitlab.com/mastiff/mock-firestore/commits/master)
[![coverage report](https://gitlab.com/mastiff/mock-firestore/badges/master/coverage.svg)](https://gitlab.com/mastiff/mock-firestore/commits/master)

> Mock Firestore implementation to aid testing of application logic and security rules

## Table of Contents

- [Security](#security)
- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [API](#api)
- [Contribute](#contribute)
- [License](#license)

## Getting Started

### Project Requirements

* [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
* [NVM](https://github.com/creationix/nvm) [Optional] Node must be installed globally to use resin local for testing. If you
use NVM, run `n=$(which node);n=${n%/bin/node}; chmod -R 755 $n/bin/*; sudo cp -r $n/{bin,lib,share} /usr/local` to copy Node
to /usr/local
* [Node and NPM](https://nodejs.org/en/download/).
* [Yarn](https://yarnpkg.com/lang/en/docs/install) [Optional]
* [Service account key](https://cloud.google.com/iam/docs/creating-managing-service-account-keys)

## Usage
TODO

### Roadmap
- 100% code coverage
- Improve security rule testing for Batch and Transaction operations
- Improve documentation
- Add samples

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/mastiff/lurcher/tags). 

## Authors

* [Eric Smyth](https://gitlab.com/smythian)

## License
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
