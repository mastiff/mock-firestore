/*
 * Copyright (c) 2018 Mastiff
 *
 * This file is licensed under the MIT license found in the LICENSE file in the
 * root directory of this source tree.
 *
 */

global.serviceAccount = require('./service-account.json');

process.on('unhandledRejection', (reason, promise) => {
  // eslint-disable-next-line no-console
  console.log('Unhandled Rejection at: Promise', promise, 'reason:', reason);
});
