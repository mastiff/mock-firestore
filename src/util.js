/*
 * Copyright (c) 2018 Mastiff
 *
 * This file is licensed under the MIT license found in the LICENSE file in the
 * root directory of this source tree.
 *
 */

const alpha = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

const genId = (len = 20) => Array(len).fill(undefined)
  .map(() => alpha[Math.floor(Math.random() * alpha.length)])
  .reduce((acc, cv) => `${acc}${cv}`, '');


const compareInputs = (actual, expected) => actual.length === expected.length
  && actual.every((el) => expected.includes(el));

const hasInputs = (actual, allowed) => actual.every((el) => allowed.includes(el));

const notChanged = (keys, oldData, newData) => keys
// eslint-disable-next-line security/detect-object-injection
  .reduce((result, key) => result && oldData[key] === newData[key], true);

const isEmptyObject = (obj) => (Object.keys(obj).length === 0 && obj.constructor === Object);


module.exports = {
  genId,
  compareInputs,
  hasInputs,
  notChanged,
  isEmptyObject,
};
