/*
 * Copyright (c) 2018 Mastiff
 *
 * This file is licensed under the MIT license found in the LICENSE file in the
 * root directory of this source tree.
 *
 */

/* eslint-disable no-underscore-dangle */
const assert = require('assert');

const Resource = require('./Resource');

const Request = (method, documentOrCollection, auth, newData = null) => {
  assert(['get', 'list', 'create', 'update', 'delete'].includes(method));
  assert(['create', 'update'].includes(method) ? !!newData : !newData);
  // todo add time property
  // todo add query property

  const result = {
    auth,
    method,
    path: `/databases/(default)/documents${documentOrCollection._path}`,
  };

  if (['create', 'update', 'delete'].includes(method)) {
    const newKeys = Object.keys(newData);
    const oldKeys = Object.keys(documentOrCollection._data);
    result.resource = Resource(documentOrCollection, newData);
    result.writeFields = oldKeys
      // eslint-disable-next-line security/detect-object-injection
      .filter((key) => !(newKeys.includes(key) && documentOrCollection._data[key] === newData[key]));
    newKeys.forEach((key) => {
      if (!oldKeys.includes(key)) {
        result.writeFields.push(key);
      }
    });
  }

  return result;
};

module.exports = Request;
