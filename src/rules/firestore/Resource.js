/*
 * Copyright (c) 2018 Mastiff
 *
 * This file is licensed under the MIT license found in the LICENSE file in the
 * root directory of this source tree.
 *
 */

/* eslint-disable no-underscore-dangle */

const Resource = (documentOrCollection, data = null) => ({
  __name__: `/databases/(default)/documents${documentOrCollection._path}`,
  id: documentOrCollection.id,
  data: data || documentOrCollection._data,
});

module.exports = Resource;
