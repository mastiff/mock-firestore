/*
 * Copyright (c) 2018 Mastiff
 *
 * This file is licensed under the MIT license found in the LICENSE file in the
 * root directory of this source tree.
 *
 */

const Request = require('./Request');
const Resource = require('./Resource');

module.exports = {
  Request,
  Resource,
};
