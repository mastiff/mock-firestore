/*
 * Copyright (c) 2018 Mastiff
 *
 * This file is licensed under the MIT license found in the LICENSE file in the
 * root directory of this source tree.
 *
 */

const firestore = require('./firestore/index');

module.exports = {
  firestore,
};
