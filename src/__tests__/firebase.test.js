/*
 * Copyright (c) 2018 Mastiff
 *
 * This file is licensed under the MIT license found in the LICENSE file in the
 * root directory of this source tree.
 *
 */

const fb = require('../firebase');
const firestore = require('../firestore/index');

const app = fb.initializeApp();

describe('Test firebase mock', () => {
  it('firebase.firestore is a mock firestore', () => expect(fb.firestore)
    .toEqual(firestore));

  it('app.firestore is a fn to get firestore instance', () => expect(app.firestore)
    .toEqual(firestore.FirestoreMock.getFirestore));
});
