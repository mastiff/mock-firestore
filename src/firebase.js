/*
 * Copyright (c) 2018 Mastiff
 *
 * This file is licensed under the MIT license found in the LICENSE file in the
 * root directory of this source tree.
 *
 */

// https://firebase.google.com/docs/reference/js/firebase
const firebase = jest.genMockFromModule('firebase');

const firestore = require('./firestore/index');

firebase.firestore = firestore;
firebase.initializeApp = jest.fn(() => ({
  firestore: firestore.FirestoreMock.getFirestore,
}));

module.exports = firebase;
