/*
 * Copyright (c) 2018 Mastiff
 *
 * This file is licensed under the MIT license found in the LICENSE file in the
 * root directory of this source tree.
 *
 */

const assert = require('assert');
const { google } = require('googleapis');

const FirestoreError = require('./FirestoreError');
const rules = require('../rules/index');

let firebaseRules;
let projectId;

const initialize = () => {
  // eslint-disable-next-line camelcase
  const { client_email: email, private_key: privateKey, project_id } = global.serviceAccount;
  const auth = new google.auth.JWT(
    email,
    null,
    privateKey,
    ['https://www.googleapis.com/auth/firebase.readonly'],
    null
  );
  // eslint-disable-next-line camelcase
  projectId = project_id;

  firebaseRules = google.firebaserules({ version: 'v1', auth });
};

const testRule = (
  firestoreRules,
  method,
  expectation,
  documentOrCollection,
  auth = {},
  newData = null
) => new Promise((resolve, reject) => {
  assert(expectation === 'ALLOW' || expectation === 'DENY');
  const docs = method === 'list'
    // eslint-disable-next-line no-underscore-dangle
    ? documentOrCollection._docs
    : [documentOrCollection];

  firebaseRules.projects.test({
    name: `projects/${projectId}`,
    resource: {
      source: { files: [{ name: 'firestore.rules', content: firestoreRules }] },
      testSuite: {
        testCases: docs.map((doc) => ({
          expectation,
          request: rules.firestore.Request(method, doc, auth, newData),
          resource: rules.firestore.Resource(doc),
        }))
      },
    }
  }, null, (err, results) => {
    if (err) {
      reject(err);
    } else {
      const failed = results.data.testResults
        .filter((result) => result.state === 'FAILURE')
        .reduce((acc, cv) => cv, false);

      if (failed) {
        const { debugMessages, visitedExpressions } = failed;
        let errorCode = 'unknown';
        let failureMessage;
        if (debugMessages) {
          errorCode = 'permission-denied';
          failureMessage = debugMessages.join('\n');
        } else if (visitedExpressions) {
          errorCode = 'permission-denied';
          failureMessage = visitedExpressions
            .map(({ sourcePosition: sp, value }) => `Line ${sp.line} column ${sp.column} evaluated to ${value}`)
            .join('\n');
        }
        reject(new FirestoreError(errorCode, failureMessage));
      } else {
        resolve(true);
      }
    }
  });
});

module.exports = {
  initialize,
  testRule
};
