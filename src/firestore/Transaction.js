/*
 * Copyright (c) 2018 Mastiff
 *
 * This file is licensed under the MIT license found in the LICENSE file in the
 * root directory of this source tree.
 *
 */

/* eslint-disable no-underscore-dangle */
const FirestoreMock = require('./FirestoreMock');

class Transaction {
  constructor() {
    this._operations = [];
    this._hasModified = false;

    this.commit = jest.fn(Transaction.prototype._commit);
    this.delete = jest.fn(Transaction.prototype._delete);
    this.get = jest.fn(Transaction.prototype._get);
    this.set = jest.fn(Transaction.prototype._set);
    this.update = jest.fn(Transaction.prototype._update);
  }

  _commit() {
    const promises = [];

    this._operations.forEach(({ action, documentRef, args = [] }) => {
      // eslint-disable-next-line security/detect-object-injection
      promises.push(documentRef[action](...args));
    });

    return Promise.all(promises);
  }

  _delete(documentRef) {
    FirestoreMock.checkThrow();
    this._hasModified = true;
    this._operations.push({ documentRef, action: 'delete' });
    return this;
  }

  _get(documentRef) {
    FirestoreMock.checkThrow();
    if (this._hasModified) { throw new Error('Data already modified'); }
    return Promise.resolve(documentRef.get());
  }

  _set(documentRef, data, options) {
    FirestoreMock.checkThrow();
    this._hasModified = true;
    const operation = { documentRef, action: 'set', args: [data] };
    if (options) { operation.args.push(options); }
    this._operations.push(operation);
    return this;
  }

  _update(documentRef, data) {
    FirestoreMock.checkThrow();
    this._hasModified = true;
    this._operations.push({ documentRef, action: 'update', args: [data] });
    return this;
  }

  mockClear() {
    this.get.mockClear();
    this.set.mockClear();
    this.update.mockClear();
    this.delete.mockClear();
    this.commit.mockClear();
  }
}

module.exports = Transaction;
