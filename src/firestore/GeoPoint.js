/*
 * Copyright (c) 2018 Mastiff
 *
 * This file is licensed under the MIT license found in the LICENSE file in the
 * root directory of this source tree.
 *
 */

class GeoPoint {
  constructor(latitude, longitude) {
    const that = this;
    this.latitude = latitude;
    this.longitude = longitude;

    this.isEqual = jest.fn((other) => other instanceof GeoPoint
        && other.latitude === that.latitude
        && other.longitude === that.longitude);

    this.mockClear = this.isEqual.mockClear;
  }
}

module.exports = GeoPoint;
