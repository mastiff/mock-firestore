/*
 * Copyright (c) 2018 Mastiff
 *
 * This file is licensed under the MIT license found in the LICENSE file in the
 * root directory of this source tree.
 *
 */
const Blob = require('./Blob');
const CollectionReference = require('./CollectionReference');
const DocumentReference = require('./DocumentReference');
const DocumentSnapshot = require('./DocumentSnapshot');
const FieldPath = require('./FieldPath');
const FieldValue = require('./FieldValue');
const Firestore = require('./Firestore');
const FirestoreMock = require('./FirestoreMock');
const GeoPoint = require('./GeoPoint');
const Query = require('./Query');
const QueryDocumentSnapshot = require('./QueryDocumentSnapshot');
const QuerySnapshot = require('./QuerySnapshot');
const Timestamp = require('./Timestamp');
const Transaction = require('./Transaction');
const WriteBatch = require('./WriteBatch');

const firestoreExport = () => FirestoreMock.getFirestore();

class FirestoreService {
  constructor() {
    this.INTERNAL = jest.fn(() => Promise.resolve());
    this.firestoreClient = FirestoreMock.getFirestore();
  }
}

firestoreExport.Firestore = FirestoreMock.getFirestore;
firestoreExport.FirestoreMock = FirestoreMock;
firestoreExport.FirestoreService = FirestoreService;
firestoreExport.registerFirestore = (firebaseInstance) => {
  firebaseInstance.INTERNAL.registerService(
    'firestore',
    FirestoreMock.getFirestore,
    Object.assign({}, {
      Blob,
      CollectionReference,
      DocumentReference,
      DocumentSnapshot,
      FieldPath,
      FieldValue,
      Firestore,
      GeoPoint,
      Query,
      QueryDocumentSnapshot,
      QuerySnapshot,
      Timestamp,
      Transaction,
      WriteBatch,
      setLogLevel: Firestore.setLogLevel
    })
  );
};

module.exports = firestoreExport;
