/*
 * Copyright (c) 2018 Mastiff
 *
 * This file is licensed under the MIT license found in the LICENSE file in the
 * root directory of this source tree.
 *
 */

/* eslint-disable no-underscore-dangle */

const assert = require('assert');

// https://firebase.google.com/docs/reference/js/firebase.firestore.Blob
class Blob {
  constructor(blob) {
    assert(blob instanceof Buffer);

    this._blob = blob;
  }

  static fromBase64String(base64) {
    // eslint-disable-next-line security/detect-unsafe-regex
    assert(/^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)$/i.test(base64));

    return new Blob(Buffer.from(base64, 'base64'));
  }

  static fromUint8Array(array) {
    assert(array instanceof Uint8Array);

    return new Blob(Buffer.from(array));
  }

  isEqual(other) {
    return other && other instanceof Blob && this._blob.equals(other._blob);
  }

  toBase64() {
    return this._blob.toString('base64');
  }

  toUint8Array() {
    return Uint8Array.from(this._blob);
  }
}

module.exports = Blob;
