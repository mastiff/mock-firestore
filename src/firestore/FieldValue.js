/*
 * Copyright (c) 2018 Mastiff
 *
 * This file is licensed under the MIT license found in the LICENSE file in the
 * root directory of this source tree.
 *
 */

class FieldValue {
  constructor() {
    throw new Error('Not implemented');
  }
}

module.exports = FieldValue;
