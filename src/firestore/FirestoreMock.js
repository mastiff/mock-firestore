/*
 * Copyright (c) 2018 Mastiff
 *
 * This file is licensed under the MIT license found in the LICENSE file in the
 * root directory of this source tree.
 *
 */
const { initialize, testRule } = require('./validationHelper');

let firestoreInstance = null;
const buildFirestoreInstanceData = () => ({
  throwNext: false,
  shouldValidate: false,
  authToken: {},
});
let mockFirestoreData = buildFirestoreInstanceData();
let firestoreRules;

class FirestoreMock {
  static setFirestoreRules(rules) {
    firestoreRules = rules;
  }

  static validateAction(method, documentOrCollection, newData) {
    const { authToken, shouldValidate } = mockFirestoreData;

    return shouldValidate
      ? testRule(firestoreRules, method, 'ALLOW', documentOrCollection, authToken, newData)
      : Promise.resolve();
  }

  static setAuthToken(token = {}) {
    mockFirestoreData.authToken = token;
  }

  static setShouldValidate(state) {
    if (state) {
      initialize();
      mockFirestoreData.shouldValidate = true;
    } else {
      mockFirestoreData.shouldValidate = false;
    }
  }

  static throwNextCall() {
    mockFirestoreData.throwNext = true;
  }

  static checkThrow() {
    if (mockFirestoreData.throwNext) {
      mockFirestoreData.throwNext = false;
      throw new Error('You told me to throw');
    }
  }

  static checkThrowPromise() {
    let result = Promise.resolve();

    if (mockFirestoreData.throwNext) {
      mockFirestoreData.throwNext = false;
      result = Promise.reject(new Error('I was told to reject'));
    }

    return result;
  }

  static getFirestore() {
    if (!firestoreInstance) {
      const Firestore = require('./Firestore'); // eslint-disable-line global-require
      firestoreInstance = new Firestore();
    }

    return firestoreInstance;
  }

  static mockReset() {
    const instance = FirestoreMock.getFirestore();
    instance.mockReset();
    mockFirestoreData = buildFirestoreInstanceData();
  }
}

module.exports = FirestoreMock;
