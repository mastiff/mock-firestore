/*
 * Copyright (c) 2018 Mastiff
 *
 * This file is licensed under the MIT license found in the LICENSE file in the
 * root directory of this source tree.
 *
 */

/* eslint-disable no-underscore-dangle */
const DocumentReference = require('./DocumentReference');
const FirestoreMock = require('./FirestoreMock');
const Query = require('./Query');
const DocumentChange = require('./DocumentChange');
const { genId } = require('../util');

// https://firebase.google.com/docs/reference/js/firebase.firestore.CollectionReference
class CollectionReference extends Query {
  constructor(parent, id = genId()) {
    super(parent.firestore);

    // Implementation properties
    this.id = id;
    this.parent = parent;
    this.firestore = parent.firestore;

    // Mock properties
    this._path = `${parent._path}/${id}`;

    this.add = jest.fn(CollectionReference.prototype._add);
    this.doc = jest.fn(CollectionReference.prototype._doc);
    this.isEqual = jest.fn(CollectionReference.prototype._isEqual);
  }

  _onDocumentSnapshot(snap) {
    const doc = snap.ref;
    const changes = [];

    if (this._docs.includes(doc)) {
      const index = this._docs.indexOf(doc);
      if (snap.exists) {
        changes.push(new DocumentChange(doc, 'modified', index)); // todo implement old index
      } else {
        changes.push(new DocumentChange(doc, 'removed', -1, index));
        this._docs.splice(this._docs.indexOf(this), 1);
      }
    } else if (snap.exists) {
      this._docs.push(doc);
      changes.push(new DocumentChange(doc, 'added', this._docs.indexOf(doc), -1));
    }

    this._notifyWatchers(changes);
  }

  _add(data = {}) {
    const result = new DocumentReference(this);
    return result.set(data)
      .then(() => {
        result.onSnapshot(this._onDocumentSnapshot.bind(this));
        return result;
      });
  }

  _doc(docId = genId()) {
    FirestoreMock.checkThrow();
    const parsedDocId = docId.replace(/(^\{|\}$)/g, '');
    let result = this._docs.filter((doc) => doc.id === parsedDocId)
      .reduce((acc, cv) => cv, null);

    if (!result) {
      result = new DocumentReference(this, docId);
      result.onSnapshot(this._onDocumentSnapshot.bind(this));
    }

    return result;
  }

  _isEqual(other) {
    return other && other instanceof CollectionReference && other.id === this.id;
  }

  mockClear() {
    super.mockClear();
    this.add.mockClear();
    this.doc.mockClear();
    this.isEqual.mockClear();
  }
}

module.exports = CollectionReference;
