/*
 * Copyright (c) 2018 Mastiff
 *
 * This file is licensed under the MIT license found in the LICENSE file in the
 * root directory of this source tree.
 *
 */

/* eslint-disable no-underscore-dangle */
const assert = require('assert');

const CollectionReference = require('./CollectionReference');
const DocumentReference = require('./DocumentReference');
const FirestoreError = require('./FirestoreError');
const FirestoreMock = require('./FirestoreMock');
const Transaction = require('./Transaction');
const WriteBatch = require('./WriteBatch');

class Firestore {
  static databaseIdFromApp() { throw new FirestoreError(FirestoreError.codes.unimplemented); }

  constructor(app = '[DEFAULT]') {
    // Implementation properties
    this.logLevel = 'debug';
    this.app = app;

    // Mock properties
    this.firestore = this;
    this._networkEnabled = true;
    this._persistant = false;
    this._collections = [];
    this._firestoreSettings = null;
    this._CollectionReference = CollectionReference;
    this._path = '';

    this.batch = jest.fn(Firestore.prototype._batch);
    this.collection = jest.fn(Firestore.prototype._collection);
    this.disableNetwork = jest.fn(Firestore.prototype._disableNetwork);
    this.doc = jest.fn(Firestore.prototype._doc);
    this.disableNetwork = jest.fn(Firestore.prototype._enableNetwork);
    this.enablePersistence = jest.fn(Firestore.prototype._enablePersistence);
    this.runTransaction = jest.fn(Firestore.prototype._runTransaction);
    this.setLogLevel = jest.fn(Firestore.prototype._setLogLevel);
    this.settings = jest.fn(Firestore.prototype._settings);
  }

  _batch() {
    FirestoreMock.checkThrow();
    return new WriteBatch(this);
  }

  _collection(pathString = '') {
    FirestoreMock.checkThrow();
    return this._getHelper(CollectionReference, pathString);
  }

  _disableNetwork() {
    const that = this;
    return new Promise((resolve) => {
      FirestoreMock.checkThrow();

      that._networkEnabled = false;
      resolve();
    });
  }

  _doc(pathString = '') {
    FirestoreMock.checkThrow();
    return this._getHelper(DocumentReference, pathString);
  }

  _enableNetwork() {
    const that = this;
    return new Promise((resolve) => {
      FirestoreMock.checkThrow();

      that._networkEnabled = true;
      resolve();
    });
  }

  _enablePersistence() { // todo reject if called after other methods (other than settings)
    const that = this;
    return new Promise((resolve) => {
      FirestoreMock.checkThrow();

      that._persistant = true;
      resolve();
    });
  }

  // eslint-disable-next-line class-methods-use-this
  _runTransaction(fn) {
    return new Promise((resolve, reject) => {
      FirestoreMock.checkThrow();
      const tx = new Transaction();
      let txResult;

      fn(tx).then((result) => {
        txResult = result;
        return tx.commit();
      }).then(() => { resolve(txResult); return null; })
        .catch((err) => { reject(err); });
    });
  }

  _setLogLevel(logLevel) {
    FirestoreMock.checkThrow();
    assert(['debug', 'error', 'silent'].includes(logLevel), 'Invalid log level');
    this.logLevel = logLevel;
  }

  _settings(settings) {
    FirestoreMock.checkThrow();
    this._firestoreSettings = settings;
  }

  _getHelper(type, pathString = '') {
    const segments = pathString.split('/')
      .filter((segment) => segment.length > 0);

    assert(segments.length % 2 === 0
      ? type === DocumentReference
      : type === CollectionReference, `Incorrect type '${type}' for the number of segments '${segments.length}'`);

    return segments
      .reduce((acc, cv, index) => {
        let result;

        if (acc === null) {
          result = this._collections
            .filter((collection) => collection.id === cv)
            .reduce((def, collection) => collection, null);

          if (!result) {
            result = new CollectionReference(this, cv);
            this._collections.push(result);
          }
        } else {
          result = index % 2 === 0
            ? acc.collection(cv)
            : acc.doc(cv);
        }

        return result;
      }, null);
  }

  mockClear() {
    this.batch.mockClear();
    this.collection.mockClear();
    this.disableNetwork.mockClear();
    this.doc.mockClear();
    this.disableNetwork.mockClear();
    this.enablePersistence.mockClear();
    this.runTransaction.mockClear();
    this.setLogLevel.mockClear();
    this.settings.mockClear();
  }

  mockReset() {
    this.mockClear();
    this._collections.length = 0;
  }
}

module.exports = Firestore;
