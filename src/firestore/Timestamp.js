/*
 * Copyright (c) 2018 Mastiff
 *
 * This file is licensed under the MIT license found in the LICENSE file in the
 * root directory of this source tree.
 *
 */

class Timestamp {
  static fromMillis(ms) {
    return new Timestamp(Math.floor(ms / 1000), (ms % 1000) * 1000000);
  }

  static fromDate(date) {
    return Timestamp.fromMillis(date.getTime());
  }

  static now() {
    return Timestamp.fromMillis(new Date().getTime());
  }

  constructor(seconds, nanoseconds) {
    this.seconds = seconds;
    this.nanoseconds = nanoseconds;

    this.toMillis = () => (this.seconds * 1000) + (this.nanoseconds / 100000);

    this.toDate = () => new Date(this.toMillis());
  }
}

module.exports = Timestamp;
