/*
 * Copyright (c) 2018 Mastiff
 *
 * This file is licensed under the MIT license found in the LICENSE file in the
 * root directory of this source tree.
 *
 */

/* eslint-disable no-underscore-dangle */

const FirestoreMock = require('../FirestoreMock');
const Query = require('../Query');
const QuerySnapshot = require('../QuerySnapshot');

describe('Test Query', () => {
  let firestore;
  let startingPoint;
  let docs;

  beforeAll(() => {
    firestore = FirestoreMock.getFirestore();
    jest.useFakeTimers();
  });

  beforeEach(() => {
    FirestoreMock.mockReset();
    startingPoint = firestore.collection('StartingPoint');
    return Promise.all([
      startingPoint.add({ value: 1 }),
      startingPoint.add({ value: 7 }),
      startingPoint.add({ value: 10 }),
      startingPoint.add({ value: 2 }),
      startingPoint.add({ value: 2 }),
      startingPoint.add({ value: 4 }),
    ]).then((results) => {
      docs = results;
      return null;
    });
  });

  it('Test constructor', () => {
    const query = startingPoint.orderBy('value');
    expect(startingPoint).toBeInstanceOf(Query);
    expect(query.firestore).toEqual(firestore);
  });

  it('Test endAt', () => {
    const query = startingPoint.orderBy('value').endAt(4);
    expect(query._docs).toHaveLength(4);
    expect(query._docs).toEqual([docs[0], docs[3], docs[4], docs[5]]);
  });

  it('Test endBefore', () => {
    const query = startingPoint.orderBy('value').endBefore(4);
    expect(query._docs).toHaveLength(3);
    expect(query._docs).toEqual([docs[0], docs[3], docs[4]]);
  });

  it('Test get', () => {
    expect.assertions(2 + docs.length);
    return startingPoint.get()
      .then((querySnap) => {
        expect(querySnap).toBeInstanceOf(QuerySnapshot);
        expect(querySnap.size).toEqual(docs.length);
        querySnap.forEach((docSnap, index) => {
          // eslint-disable-next-line security/detect-object-injection
          expect(docSnap.ref).toEqual(docs[index]);
        });
        return null;
      });
  });

  it('Test limit', () => {
    const query = startingPoint.limit(2);
    expect(query._docs).toHaveLength(2);
    expect(query._docs).toEqual([docs[0], docs[1]]);
  });

  it('Test orderBy desc', () => {
    const query = startingPoint.orderBy('value', 'desc');
    expect(query._docs).toEqual([docs[2], docs[1], docs[5], docs[3], docs[4], docs[0]]);
  });

  it('Test orderBy asc', () => {
    const query = startingPoint.orderBy('value');
    expect(query._docs).toEqual([docs[0], docs[3], docs[4], docs[5], docs[1], docs[2]]);
  });

  it('Test startAfter', () => {
    const query = startingPoint.orderBy('value').startAfter(4);
    expect(query).toBeInstanceOf(Query);
    expect(query.firestore).toEqual(firestore);
    expect(query._docs).toHaveLength(2);
    expect(query._docs).toEqual([docs[1], docs[2]]);
  });

  it('Test startAt', () => {
    const query = startingPoint.orderBy('value').startAt(4);
    expect(query).toBeInstanceOf(Query);
    expect(query.firestore).toEqual(firestore);
    expect(query._docs).toHaveLength(3);
    expect(query._docs).toEqual([docs[5], docs[1], docs[2]]);
  });

  it('Test where <', () => {
    const query = startingPoint.where('value', '<', 4);
    expect(query._docs).toEqual([docs[0], docs[3], docs[4]]);
  });

  it('Test where <=', () => {
    const query = startingPoint.where('value', '<=', 4);
    expect(query._docs).toEqual([docs[0], docs[3], docs[4], docs[5]]);
  });
  it('Test where ==', () => {
    const query = startingPoint.where('value', '==', 2);
    expect(query._docs).toEqual([docs[3], docs[4]]);
  });

  it('Test where >', () => {
    const query = startingPoint.where('value', '>', 4);
    expect(query._docs).toEqual([docs[1], docs[2]]);
  });

  it('Test where >=', () => {
    const query = startingPoint.where('value', '>=', 4);
    expect(query._docs).toEqual([docs[1], docs[2], docs[5]]);
  });

  it('Test where no matches', () => {
    const query = startingPoint.where('price', '>=', 4);
    expect(query._docs).toEqual([]);
  });

  it('Test where invalid operation', () => {
    expect(() => startingPoint.where('value', '<>', 4)).toThrow('Invalid where operation');
  });

  it('Test onSnapshot', () => {
    expect.assertions(3 + (3 * docs.length));
    const onSnap = jest.fn((snap) => {
      const changes = snap.docChanges();
      expect(changes).toHaveLength(docs.length);
      expect(onSnap).toHaveBeenCalledTimes(1);
      expect(snap).toBeInstanceOf(QuerySnapshot);

      snap.forEach((qds, index) => {
        // eslint-disable-next-line security/detect-object-injection
        expect(qds.ref).toEqual(docs[index]);
      });

      changes.forEach((change, index) => {
        expect(change.type).toEqual('added');
        // eslint-disable-next-line security/detect-object-injection
        expect(change.doc.ref).toEqual(docs[index]);
      });
    });
    startingPoint.onSnapshot(onSnap);
  });

  it('Test onSnapshot modified', () => {
    expect.assertions(5 + docs.length);
    let snap;
    const onSnap = jest.fn((s) => { snap = s; });
    startingPoint.onSnapshot(onSnap);
    return docs[0].update({ maze: 'is not meant for you' })
      .then(() => {
        expect(onSnap).toHaveBeenCalledTimes(2);
        expect(snap).toBeInstanceOf(QuerySnapshot);
        // eslint-disable-next-line security/detect-object-injection
        snap.forEach((qds, index) => { expect(qds.ref).toEqual(docs[index]); });
        const changes = snap.docChanges();
        expect(changes).toHaveLength(1);
        expect(changes[0].type).toEqual('modified');
        expect(changes[0].doc.ref).toEqual(docs[0]);
        return null;
      });
  });

  it('Test onSnapshot added', () => {
    expect.assertions(6 + docs.length);
    let snap;
    const onSnap = jest.fn((s) => { snap = s; });
    startingPoint.onSnapshot(onSnap);
    return startingPoint.add({ value: 15 })
      .then((newDoc) => {
        expect(onSnap).toHaveBeenCalledTimes(2);
        expect(snap).toBeInstanceOf(QuerySnapshot);
        // eslint-disable-next-line security/detect-object-injection
        docs.forEach((doc, index) => { expect(snap.docs[index].ref).toEqual(doc); });
        expect(snap.docs[snap.docs.length - 1].ref).toEqual(newDoc);
        const changes = snap.docChanges();
        expect(changes).toHaveLength(1);
        expect(changes[0].type).toEqual('added');
        expect(changes[0].doc.ref).toEqual(newDoc);
        return null;
      });
  });

  it('Test onSnapshot added via doc set', () => {
    expect.assertions(10);
    let snap;
    const onSnap = jest.fn((s) => {
      snap = s;
    });
    const newDoc = startingPoint.doc();
    startingPoint.onSnapshot(onSnap);
    onSnap.mockClear();
    return docs[0].update({ maze: 'is not meant for you' })
      .then(() => {
        expect(onSnap).toHaveBeenCalledTimes(1);
        expect(snap).toBeInstanceOf(QuerySnapshot);
        // QuerySnapshot should not include `newDoc` since it doesn't exist
        expect(snap.docs).toHaveLength(docs.length);
        return newDoc.set({ value: 14 });
      })
      .then(() => {
        expect(onSnap).toHaveBeenCalledTimes(2);
        expect(snap).toBeInstanceOf(QuerySnapshot);
        expect(snap.docs).toHaveLength(docs.length + 1);
        expect(snap.docs[snap.docs.length - 1].ref).toEqual(newDoc);
        const changes = snap.docChanges();
        expect(changes).toHaveLength(1);
        expect(changes[0].type).toEqual('added');
        expect(changes[0].doc.ref).toEqual(newDoc);
        return null;
      });
  });

  it('Test onSnapshot deleted', () => {
    expect.assertions(4 + docs.length);
    let snap;
    const onSnap = jest.fn((s) => { snap = s; });
    startingPoint.onSnapshot(onSnap);
    onSnap.mockClear();
    return docs[docs.length - 1].delete()
      .then(() => {
        expect(onSnap).toHaveBeenCalledTimes(1);
        expect(snap).toBeInstanceOf(QuerySnapshot);
        // eslint-disable-next-line security/detect-object-injection
        snap.forEach((qds, index) => { expect(qds.ref).toEqual(docs[index]); });
        const changes = snap.docChanges();
        expect(changes).toHaveLength(1);
        expect(changes[0].type).toEqual('removed');
        expect(changes[0].doc.ref).toEqual(docs[docs.length - 1]);
        return null;
      });
  });

  it('Test onSnapshot deleted in parent query', () => {
    expect.assertions(8);
    let snap;
    const onSnap = jest.fn((s) => { snap = s; });
    startingPoint.orderBy('value').limit(1).onSnapshot(onSnap);
    onSnap.mockClear();
    return docs[0].delete()
      .then(() => {
        expect(onSnap).toHaveBeenCalledTimes(1);
        expect(snap).toBeInstanceOf(QuerySnapshot);
        expect(snap.docs[0].ref).toEqual(docs[3]);
        const changes = snap.docChanges();
        expect(changes).toHaveLength(2);
        expect(changes[0].type).toEqual('added');
        expect(changes[0].doc.ref).toEqual(docs[3]);
        expect(changes[1].type).toEqual('removed');
        expect(changes[1].doc.ref).toEqual(docs[0]);
        return null;
      });
  });

  it('Test unsubscribe onSnapshot', () => {
    expect.assertions(4);
    let snap = null;
    const onSnap = jest.fn((s) => { snap = s; });
    const toUnsub = startingPoint.onSnapshot(onSnap);
    expect(startingPoint._watchers).toEqual([expect.any(Function)]);
    toUnsub();
    expect(startingPoint._watchers).toEqual([]);
    return startingPoint.add({ value: 15 })
      .then(() => {
        expect(onSnap).toHaveBeenCalledTimes(1);
        expect(snap.size).toEqual(docs.length);
        return null;
      });
  });


  it('Test mockClear', () => {
    const toClear = firestore.collection('ToClear');
    expect.assertions(9);
    expect(toClear.add).toHaveBeenCalledTimes(0);
    expect(toClear.doc).toHaveBeenCalledTimes(0);
    expect(toClear.isEqual).toHaveBeenCalledTimes(0);
    return toClear.add({ foo: 'bar' })
      .then(() => {
        toClear.doc('abc');
        toClear.isEqual(null);
        expect(toClear.add).toHaveBeenCalledTimes(1);
        expect(toClear.doc).toHaveBeenCalledTimes(1);
        expect(toClear.isEqual).toHaveBeenCalledTimes(1);
        toClear.mockClear();
        expect(toClear.add).toHaveBeenCalledTimes(0);
        expect(toClear.doc).toHaveBeenCalledTimes(0);
        expect(toClear.isEqual).toHaveBeenCalledTimes(0);
        return null;
      });
  });

  it('Test mock reject', () => {
    const query = new Query(firestore);
    FirestoreMock.throwNextCall();
    expect(query.get()).rejects.toThrow('I was told to reject');
  });
});
