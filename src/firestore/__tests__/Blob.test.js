/*
 * Copyright (c) 2018 Mastiff
 *
 * This file is licensed under the MIT license found in the LICENSE file in the
 * root directory of this source tree.
 *
 */

/* eslint-disable no-underscore-dangle */

const Blob = require('../Blob');

describe('Test Blob', () => {
  it('Test from base64 to Uint8array', () => {
    const blob = Blob.fromBase64String('SGVsbG8gd29ybGQ=');
    expect(blob).toBeInstanceOf(Blob);
    expect(blob._blob).toEqual(Buffer.from([72, 101, 108, 108, 111, 32, 119, 111, 114, 108, 100]));
    expect(blob.toUint8Array()).toEqual(Uint8Array.from([72, 101, 108, 108, 111, 32, 119, 111, 114, 108, 100]));
  });

  it('Test from Uint8array to base64', () => {
    const blob = Blob.fromUint8Array(Uint8Array.from([72, 101, 108, 108, 111, 32, 119, 111, 114, 108, 100]));
    expect(blob).toBeInstanceOf(Blob);
    expect(blob._blob).toEqual(Buffer.from([72, 101, 108, 108, 111, 32, 119, 111, 114, 108, 100]));
    expect(blob.toBase64()).toEqual('SGVsbG8gd29ybGQ=');
  });

  it('Test isEqual', () => {
    const first = Blob.fromBase64String('SGVsbG8gd29ybGQ=');
    const second = Blob.fromUint8Array(Uint8Array.from([72, 101, 108, 108, 111, 32, 119, 111, 114, 108, 100]));
    expect(first.isEqual(second)).toBeTruthy();
  });
});
