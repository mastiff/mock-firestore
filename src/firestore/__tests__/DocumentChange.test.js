/*
 * Copyright (c) 2018 Mastiff
 *
 * This file is licensed under the MIT license found in the LICENSE file in the
 * root directory of this source tree.
 *
 */
const FirestoreMock = require('../FirestoreMock');
const DocumentChange = require('../DocumentChange');
const QueryDocumentSnapshot = require('../QueryDocumentSnapshot');

describe('Test DocumentChange', () => {
  it('Test happy path', () => {
    const change = new DocumentChange(FirestoreMock.getFirestore().doc('/col/doc'), 'added', 5, 1000);
    expect(change).toBeInstanceOf(DocumentChange);
    expect(change.doc).toBeInstanceOf(QueryDocumentSnapshot);
    expect(change.newIndex).toEqual(5);
    expect(change.oldIndex).toEqual(-1);
  });

  it('Test bad doc', () => expect(() => new DocumentChange(null, 'added'))
    .toThrow('Not a valid DocumentReference'));

  it('Test bad type', () => {
    const doc = FirestoreMock.getFirestore().doc('/col/doc');
    expect(() => new DocumentChange(new QueryDocumentSnapshot(doc), 'not-a-change-type'))
      .toThrow('The type \'not-a-change-type\' is not a valid change type');
  });
});
