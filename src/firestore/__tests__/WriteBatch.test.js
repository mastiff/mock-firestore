/*
 * Copyright (c) 2018 Mastiff
 *
 * This file is licensed under the MIT license found in the LICENSE file in the
 * root directory of this source tree.
 *
 */

/* eslint-disable no-underscore-dangle */

const FirestoreMock = require('../FirestoreMock');

describe('Test WriteBatch', () => {
  let db;
  let toUpdate;
  let toSet;
  let toDelete;

  beforeAll(() => {
    db = FirestoreMock.getFirestore();
    toUpdate = db.collection('test').doc();
    toSet = db.collection('test').doc();
    toDelete = db.collection('test').doc();

    return Promise.all([
      toUpdate.set({ come: 'at the king', you: 'best not miss' }),
      toSet.set({ eat: 'my shorts', dont: 'have a cow, man' }),
      toDelete.set({ these: 'violent delights', have: 'violent ends' }),
    ]);
  });

  it('Test commit', () => {
    expect(toUpdate._data).toEqual({ come: 'at the king', you: 'best not miss' });
    expect(toSet._data).toEqual({ eat: 'my shorts', dont: 'have a cow, man' });
    expect(toDelete._data).toEqual({ these: 'violent delights', have: 'violent ends' });

    const batch = db.batch();

    batch.update(toUpdate, { omar: 'little' })
      .set(toSet, { twenty: 'dollars can buy many peanuts' })
      .delete(toDelete);

    expect(toUpdate._data).toEqual({ come: 'at the king', you: 'best not miss' });
    expect(toSet._data).toEqual({ eat: 'my shorts', dont: 'have a cow, man' });
    expect(toDelete._data).toEqual({ these: 'violent delights', have: 'violent ends' });

    return batch.commit()
      .then(() => {
        expect(toUpdate._data).toEqual({ come: 'at the king', you: 'best not miss', omar: 'little' });
        expect(toSet._data).toEqual({ twenty: 'dollars can buy many peanuts' });
        expect(toDelete._exists).toEqual(false);
        return null;
      });
  });

  it('Test commit rejects', () => {
    const batch = db.batch();
    const doc = db.doc('/batch/doc');

    batch.set(doc, { twenty: 'dollars can buy many peanuts' });
    FirestoreMock.throwNextCall();

    expect(batch.commit()).rejects.toThrow('I was told to reject');
  });
});
