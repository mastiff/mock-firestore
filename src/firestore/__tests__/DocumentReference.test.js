/*
 * Copyright (c) 2018 Mastiff
 *
 * This file is licensed under the MIT license found in the LICENSE file in the
 * root directory of this source tree.
 *
 */

/* eslint-disable no-underscore-dangle */
const FirestoreMock = require('../FirestoreMock');
const DocumentSnapshot = require('../DocumentSnapshot');

describe('Test DocumentReference', () => {
  let firestore;

  beforeAll(() => {
    firestore = FirestoreMock.getFirestore();
  });

  it('Test firestore property', () => {
    expect.assertions(4);
    const doc = firestore.doc('/collection/foo');
    expect(doc.firestore).toEqual(firestore);
    expect(doc.id).toEqual('foo');
    expect(doc.parent.id).toEqual('collection');
    return firestore.collection('collection').add({ foo: 'bar' })
      .then((ref) => expect(ref.id).toEqual(expect.stringMatching(/^[A-z0-9]{20}$/)));
  });

  it('Test update on doc that doesn\'t exist', () => {
    expect.assertions(1);
    const toUpdate = firestore.collection('test').doc();
    return expect(toUpdate.update({ come: 'at the king', you: 'best not miss' }))
      .rejects.toThrow('DocumentReference does not exist');
  });

  it('Test update on doc that exists', () => {
    expect.assertions(2);
    const toUpdate = firestore.collection('test').doc();
    return toUpdate.set({ foo: 'bar' })
      .then(() => toUpdate.update({ bar: 'foo' }))
      .then(() => expect(toUpdate._data).toEqual({ foo: 'bar', bar: 'foo' }))
      .then(() => toUpdate.get())
      .then((snap) => expect(snap.data()).toEqual({ foo: 'bar', bar: 'foo' }));
  });

  it('Test set then get', () => {
    expect.assertions(5);
    const toSet = firestore.collection('test').doc();
    return toSet.set({ eat: 'my shorts', dont: 'have a cow, man' })
      .then(() => expect(toSet._data).toEqual({ eat: 'my shorts', dont: 'have a cow, man' }))
      .then(() => toSet.get())
      .then((docSnapshot) => {
        expect(docSnapshot.ref).toEqual(toSet);
        expect(docSnapshot.data()).toEqual({ eat: 'my shorts', dont: 'have a cow, man' });
        expect(docSnapshot.get('eat')).toEqual('my shorts');
        return null;
      })
      .then(() => toSet.set({ thats: 'all folks' }))
      .then(() => expect(toSet._data).toEqual({ thats: 'all folks' }));
  });

  it('Test delete', () => {
    expect.assertions(4);
    const toDelete = firestore.collection('test').doc();
    return toDelete.set({ foo: 'bar' })
      .then(() => {
        expect(toDelete._exists).toEqual(true);
        return toDelete.delete();
      })
      .then(() => expect(toDelete._exists).toEqual(false))
      .then(() => expect(toDelete.delete()).rejects.toThrow('DocumentReference does not exist'))
      .then(() => expect(toDelete.update({ foo: 'bar' })).rejects.toThrow('DocumentReference does not exist'));
  });

  it('Test onSnapshot', () => {
    expect.assertions(4);
    let snap;
    const toSnap = firestore.collection('test').doc();
    const onSnap = jest.fn((s) => { snap = s; });
    toSnap.onSnapshot(onSnap);
    onSnap.mockClear();
    return toSnap.set({ maze: 'is not meant for you' })
      .then(() => {
        expect(onSnap).toHaveBeenCalledTimes(1);
        expect(snap).toBeInstanceOf(DocumentSnapshot);
        expect(snap.id).toEqual(expect.stringMatching(/[A-z0-9]{20}/));
        expect(snap.data()).toEqual({ maze: 'is not meant for you' });
        return null;
      });
  });

  it('Test unsubscribe onSnapshot', () => {
    expect.assertions(3);
    const toSnap = firestore.collection('test').doc();
    const onSnap = jest.fn();
    const colToUnsub = toSnap._watchers[0];
    const toUnsub = toSnap.onSnapshot(onSnap);
    toUnsub.mockClear();
    expect(toSnap._watchers).toEqual([colToUnsub, onSnap]);
    toUnsub();
    expect(toSnap._watchers).toEqual([colToUnsub]);
    return toSnap.set({ maze: 'is not meant for you' })
      .then(() => expect(onSnap).toHaveBeenCalledTimes(1));
  });

  it('Test collection', () => {
    expect.assertions(6);
    const doc = firestore.doc('test/col');
    expect(doc._collections).toEqual([]);
    const col = doc.collection('faberge');
    expect(doc._collections).toEqual([col]);
    const col2 = doc.collection('faberge');
    expect(doc._collections).toEqual([col2]);
    expect(col).toEqual(col2);
    const col3 = doc.collection('eggs');
    expect(doc._collections).toEqual([col, col3]);
    expect(col).not.toEqual(col3);
  });

  it('Test mockClear', () => {
    const doc = firestore.doc('mock/doc');
    expect.assertions(18);
    expect(doc.update).toHaveBeenCalledTimes(0);
    expect(doc.get).toHaveBeenCalledTimes(0);
    expect(doc.collection).toHaveBeenCalledTimes(0);
    expect(doc.onSnapshot).toHaveBeenCalledTimes(1); // Collection `mock` subscribes to doc
    expect(doc.delete).toHaveBeenCalledTimes(0);
    expect(doc.set).toHaveBeenCalledTimes(0);
    return doc.set({ foo: 'bar' })
      .then(() => doc.update({ foo: 'rab' }))
      .then(() => doc.get())
      .then(() => doc.collection('col'))
      .then(() => doc.onSnapshot(() => {}))
      .then(() => doc.delete())
      .then(() => {
        expect(doc.update).toHaveBeenCalledTimes(1);
        expect(doc.get).toHaveBeenCalledTimes(1);
        expect(doc.collection).toHaveBeenCalledTimes(1);
        expect(doc.onSnapshot).toHaveBeenCalledTimes(2);
        expect(doc.delete).toHaveBeenCalledTimes(1);
        expect(doc.set).toHaveBeenCalledTimes(1);
        doc.mockClear();
        expect(doc.update).toHaveBeenCalledTimes(0);
        expect(doc.get).toHaveBeenCalledTimes(0);
        expect(doc.collection).toHaveBeenCalledTimes(0);
        expect(doc.onSnapshot).toHaveBeenCalledTimes(0);
        expect(doc.delete).toHaveBeenCalledTimes(0);
        expect(doc.set).toHaveBeenCalledTimes(0);
        return null;
      });
  });

  it('Test mock reject', () => {
    expect.assertions(1);
    const doc = firestore.doc('/collection/foo');
    FirestoreMock.throwNextCall();
    expect(doc.get()).rejects.toThrow('I was told to reject');
  });
});
