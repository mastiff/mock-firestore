/*
 * Copyright (c) 2018 Mastiff
 *
 * This file is licensed under the MIT license found in the LICENSE file in the
 * root directory of this source tree.
 *
 */

/* eslint-disable no-underscore-dangle */

const FirestoreMock = require('../FirestoreMock');

describe('Test CollectionReference', () => {
  let firestore;

  beforeAll(() => {
    jest.useFakeTimers();
    firestore = FirestoreMock.getFirestore();
  });

  it('Test properties', () => {
    expect.assertions(3);
    const parent = firestore.doc('/ACollection/ADocument');
    const collection = parent.collection();
    expect(collection.firestore).toEqual(firestore);
    expect(collection.parent).toEqual(parent);
    expect(collection.id).toEqual(expect.stringMatching(/^[A-z0-9]{20}$/));
  });

  it('Test add', () => {
    expect.assertions(2);
    const toAdd = firestore.collection('ToAddCollection');
    expect(toAdd._docs).toEqual([]);
    return toAdd.add({ come: 'at the king', you: 'best not miss' })
      .then((ref) => expect(toAdd._docs).toEqual([ref]));
  });

  it('Test add default data', () => {
    expect.assertions(2);
    const toAdd = firestore.collection('ToAddDefaultCollection');
    expect(toAdd._docs).toEqual([]);
    return toAdd.add()
      .then((ref) => expect(toAdd._docs).toEqual([ref]));
  });

  it('Test doc', () => {
    expect.assertions(1);
    const toDoc = firestore.collection('ToDocCollection');
    return toDoc.add({ come: 'at the king', you: 'best not miss' })
      .then((ref) => expect(ref).toEqual(toDoc.doc(ref.id)));
  });

  it('Test doc no id provided', () => {
    expect.assertions(2);
    const doc = firestore.collection('NoIdDocCollection').doc();
    expect(doc._exists).toBeFalsy();
    expect(doc.id).toEqual(expect.stringMatching(/^[A-z0-9]{20}$/));
  });

  it('Test isEqual', () => {
    expect.assertions(1);
    const one = firestore.collection('IsEqualCollection');
    const two = firestore.collection('/IsEqualCollection');
    expect(one.isEqual(two)).toBeTruthy();
  });

  it('Test mockClear', () => {
    const toClear = firestore.collection('ToClear');
    expect.assertions(9);
    expect(toClear.add).toHaveBeenCalledTimes(0);
    expect(toClear.doc).toHaveBeenCalledTimes(0);
    expect(toClear.isEqual).toHaveBeenCalledTimes(0);
    return toClear.add({ foo: 'bar' })
      .then(() => {
        toClear.doc('abc');
        toClear.isEqual(null);
        expect(toClear.add).toHaveBeenCalledTimes(1);
        expect(toClear.doc).toHaveBeenCalledTimes(1);
        expect(toClear.isEqual).toHaveBeenCalledTimes(1);
        toClear.mockClear();
        expect(toClear.add).toHaveBeenCalledTimes(0);
        expect(toClear.doc).toHaveBeenCalledTimes(0);
        expect(toClear.isEqual).toHaveBeenCalledTimes(0);
        return null;
      });
  });

  it('Test mock reject', () => {
    expect.assertions(1);
    const doc = firestore.doc('/collection/foo');
    FirestoreMock.throwNextCall();
    expect(doc.get()).rejects.toThrow('I was told to reject');
  });
});
