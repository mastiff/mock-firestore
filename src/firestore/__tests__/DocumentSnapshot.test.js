/*
 * Copyright (c) 2018 Mastiff
 *
 * This file is licensed under the MIT license found in the LICENSE file in the
 * root directory of this source tree.
 *
 */

/* eslint-disable no-underscore-dangle,security/detect-non-literal-fs-filename */
const FirestoreMock = require('../FirestoreMock');
const DocumentSnapshot = require('../DocumentSnapshot');

describe('Test DocumentSnapshot', () => {
  it('Test happy path', () => {
    const doc = FirestoreMock.getFirestore().doc('/col/happy');
    const snap = new DocumentSnapshot(doc);
    expect(snap).toBeInstanceOf(DocumentSnapshot);
    expect(snap.ref).toEqual(doc);
    expect(snap.id).toEqual(doc.id);
    expect(snap.exists).toEqual(doc._exists);
  });

  it('Test methods', () => {
    expect.assertions(14);
    const doc = FirestoreMock.getFirestore().doc('/col/methods');

    return doc.set({ foo: 'bar', bar: 'foo' })
      .then(() => {
        const snap = new DocumentSnapshot(doc);
        expect(snap.data).toHaveBeenCalledTimes(0);
        expect(snap.get).toHaveBeenCalledTimes(0);
        expect(snap.isEqual).toHaveBeenCalledTimes(0);
        expect(snap.data()).toEqual({ foo: 'bar', bar: 'foo' });
        expect(snap.get('foo')).toEqual('bar');
        expect(snap.get('teapot')).toBeUndefined();
        expect(snap.isEqual(new DocumentSnapshot(doc))).toBeTruthy();
        expect(snap.isEqual(doc)).toBeFalsy();
        expect(snap.data).toHaveBeenCalledTimes(1);
        expect(snap.get).toHaveBeenCalledTimes(2);
        expect(snap.isEqual).toHaveBeenCalledTimes(2);
        snap.mockClear();
        expect(snap.data).toHaveBeenCalledTimes(0);
        expect(snap.get).toHaveBeenCalledTimes(0);
        expect(snap.isEqual).toHaveBeenCalledTimes(0);
        return null;
      });
  });
});
