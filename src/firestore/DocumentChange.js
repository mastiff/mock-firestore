/*
 * Copyright (c) 2018 Mastiff
 *
 * This file is licensed under the MIT license found in the LICENSE file in the
 * root directory of this source tree.
 *
 */
const assert = require('assert');

const DocumentReference = require('./DocumentReference');
const QueryDocumentSnapshot = require('./QueryDocumentSnapshot');
const FirestoreMock = require('./FirestoreMock');

// https://firebase.google.com/docs/reference/js/firebase.firestore.DocumentChange
class DocumentChange {
  constructor(doc, type, newIndex, oldIndex) {
    FirestoreMock.checkThrow();
    assert(['added', 'modified', 'removed'].includes(type), `The type '${type}' is not a valid change type`);
    assert(doc instanceof DocumentReference, 'Not a valid DocumentReference');
    this.doc = new QueryDocumentSnapshot(doc);
    this.type = type;
    this.newIndex = type === 'removed' ? -1 : newIndex;
    this.oldIndex = type === 'added' ? -1 : oldIndex;
  }
}

module.exports = DocumentChange;
