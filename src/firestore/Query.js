/*
 * Copyright (c) 2018 Mastiff
 *
 * This file is licensed under the MIT license found in the LICENSE file in the
 * root directory of this source tree.
 *
 */

/* eslint-disable no-underscore-dangle */
const assert = require('assert');

const QuerySnapshot = require('./QuerySnapshot');
const DocumentChange = require('./DocumentChange');
const DocumentSnapshot = require('./DocumentSnapshot');
const FirestoreMock = require('./FirestoreMock');
const FirestoreError = require('./FirestoreError');

// https://firebase.google.com/docs/reference/js/firebase.firestore.Query
class Query {
  constructor(firestoreOrQuery, filterFn, sortFn = () => 0) {
    // Mock properties
    this._watchers = [];
    this._docChanges = [];
    this._docChanges.first = true;
    this._docs = [];

    if (firestoreOrQuery instanceof Query) {
      this.firestore = firestoreOrQuery.firestore;
      this._query = JSON.parse(JSON.stringify(firestoreOrQuery._query));
      firestoreOrQuery.onSnapshot((snap) => {
        const removed = snap._docChanges // Removed from parent query
          .filter((dc) => dc.type === 'removed')
          .map((dc) => dc.doc.ref);
        const newDocs = snap.docs
          .map((qds) => qds.ref)
          .filter((doc) => !removed.includes(doc))
          .filter(filterFn)
          .sort(sortFn);

        const changes = newDocs
          .map((doc) => new DocumentChange(
            doc,
            this._docs.includes(doc) ? 'modified' : 'added',
            newDocs.indexOf(this),
            this._docs.indexOf(this)
          ));

        this._docs.forEach((doc) => {
          // in `removed` and docs still in parent doc set but that no longer match filters for this query
          if (!newDocs.includes(doc)) {
            changes.push(new DocumentChange(doc, 'removed', -1, this._docs.indexOf(this)));
          }
        });

        this._docs = newDocs;
        this._notifyWatchers(changes);
      });
    } else {
      this.firestore = firestoreOrQuery;
      this._query = {
        limit: -1,
        offset: null,
        orderBy: [],
      };
    }

    this.endAt = jest.fn(Query.prototype._endAt);
    this.endBefore = jest.fn(Query.prototype._endBefore);
    this.get = jest.fn(Query.prototype._get);
    this.limit = jest.fn(Query.prototype._limit);
    this.onSnapshot = jest.fn(Query.prototype._onSnapshot);
    this.orderBy = jest.fn(Query.prototype._orderBy);
    this.startAfter = jest.fn(Query.prototype._startAfter);
    this.startAt = jest.fn(Query.prototype._startAt);
    this.where = jest.fn(Query.prototype._where);
  }

  _notifyWatchers(changes) {
    if (Array.isArray(changes) && changes.length > 0) {
      const snap = new QuerySnapshot(this, changes);
      this._watchers.forEach((watcher) => watcher(snap));
    }
  }

  _getIndex(snapshotOrVarArgs, params) {
    let index;

    if (snapshotOrVarArgs instanceof DocumentSnapshot) {
      index = this._docs.findIndex((doc) => doc === snapshotOrVarArgs.ref);
    } else {
      const args = params;
      args.unshift(snapshotOrVarArgs);
      index = this._docs.findIndex((doc) => args
        // eslint-disable-next-line security/detect-object-injection
        .map((arg, idx) => doc._data[this._query.orderBy[idx].fieldPath] === arg)
        .reduce((acc, cv) => acc && cv, true));
    }

    // if (!isSnap && !this._query.orderBy) { throw new FirestoreError(FirestoreError.codes['invalid-argument']); }
    return index;
  }

  _endAt(snapshotOrVarArgs, ...params) {
    FirestoreMock.checkThrow();
    const index = this._getIndex(snapshotOrVarArgs, params);
    const result = new Query(this, (doc, idx) => idx <= index);
    result._query.offset = snapshotOrVarArgs; // todo figure out what type of clause offset should be
    return result;
  }

  _endBefore(snapshotOrVarArgs, ...params) {
    FirestoreMock.checkThrow();
    const index = this._getIndex(snapshotOrVarArgs, params);
    const result = new Query(this, (doc, idx) => idx < index);
    result._query.offset = snapshotOrVarArgs; // todo figure out what type of clause offset should be
    return result;
  }

  _get() {
    return FirestoreMock.checkThrowPromise()
      .then(() => FirestoreMock.validateAction('list', this))
      .then(() => new QuerySnapshot(this, this._docs
        .map((doc) => new DocumentChange(doc, 'added'))));
  }

  _limit(limit) {
    FirestoreMock.checkThrow();
    const result = new Query(this, (el, idx) => idx < limit);
    // todo figure out expected behavior for multiple limits e.g., query.limit(4).limit(5)
    result._query.limit = limit;
    return result;
  }

  _onSnapshot(optionsOrObserverOrOnNext, observerOrOnNextOrOnError) {
    FirestoreMock.checkThrow();
    const toCall = optionsOrObserverOrOnNext;
    const unsub = jest.fn(() => {
      this._watchers = this._watchers.filter((watcher) => watcher !== toCall);
    });
    this._watchers.push(toCall);
    toCall.__unsubscribe = unsub;
    toCall(new QuerySnapshot(this, this._docs.map((doc) => new DocumentChange(doc, 'added'))));
    // setTimeout(() => toCall(new QuerySnapshot(this, this._docs
    //   .map((doc) => new DocumentChange(doc, 'added')))), 0);

    return toCall.__unsubscribe;
  }

  _orderBy(fieldPath, directionStr = 'asc') {
    FirestoreMock.checkThrow();

    if (this._rangeField && this._rangeField !== fieldPath) { // can't have range filters on different fields
      throw new FirestoreError(FirestoreError.codes['invalid-argument']);
    }

    const result = new Query(this, () => true, (a, b) => {
      assert(['asc', 'desc'].includes(directionStr));
      const aKey = a._data[fieldPath]; // eslint-disable-line security/detect-object-injection
      const bKey = b._data[fieldPath]; // eslint-disable-line security/detect-object-injection
      const order = directionStr === 'asc' ? 1 : -1;
      let sort = 0;

      if (aKey > bKey) {
        sort = 1;
      } else if (aKey < bKey) {
        sort = -1;
      }

      return sort * order;
    });
    result._query.orderBy.push({ fieldPath, directionStr }); // todo figure out what type of clause orderBy should be

    return result;
  }

  _startAfter(snapshotOrVarArgs, ...params) {
    FirestoreMock.checkThrow();
    const index = this._getIndex(snapshotOrVarArgs, params);
    const result = new Query(this, (doc, idx) => idx > index);
    result._query.offset = snapshotOrVarArgs; // todo figure out what type of clause offset should be
    return result;
  }

  _startAt(snapshotOrVarArgs, ...params) {
    FirestoreMock.checkThrow();
    const index = this._getIndex(snapshotOrVarArgs, params);
    const result = new Query(this, (doc, idx) => idx >= index);
    result._query.offset = snapshotOrVarArgs; // todo figure out what type of clause offset should be
    return result;
  }

  _where(fieldPath, opStr, value) {
    FirestoreMock.checkThrow();
    let rangeField = this._rangeField;

    if (opStr !== '==') {
      rangeField = this._rangeField || fieldPath;

      if (rangeField !== fieldPath) { // can't have range filters on different fields
        throw new FirestoreError(FirestoreError.codes['invalid-argument']);
      }
    }

    const result = new Query(this, (doc) => {
      const docField = doc._data[fieldPath]; // eslint-disable-line security/detect-object-injection
      let include = false;

      if (Object.keys(doc._data).includes(fieldPath)) {
        switch (opStr) {
          case '<':
            include = docField < value;
            break;
          case '<=':
            include = docField <= value;
            break;
          case '==':
            include = docField === value;
            break;
          case '>':
            include = docField > value;
            break;
          case '>=':
            include = docField >= value;
            break;
          default:
            throw new Error('Invalid where operation');
        }
      }

      return include;
    });

    result._rangeField = rangeField;

    return result;
  }

  mockClear() {
    this.endAt.mockClear();
    this.endBefore.mockClear();
    this.get.mockClear();
    this.limit.mockClear();
    this.onSnapshot.mockClear();
    this.orderBy.mockClear();
    this.startAfter.mockClear();
    this.startAt.mockClear();
    this.where.mockClear();
  }
}

module.exports = Query;
