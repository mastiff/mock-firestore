/*
 * Copyright (c) 2018 Mastiff
 *
 * This file is licensed under the MIT license found in the LICENSE file in the
 * root directory of this source tree.
 *
 */

/* eslint-disable no-underscore-dangle */
const FirestoreMock = require('./FirestoreMock');

// https://firebase.google.com/docs/reference/js/firebase.firestore.DocumentSnapshot
class DocumentSnapshot {
  constructor(ref) {
    this.id = ref.id;
    this.ref = ref;
    this.exists = ref._exists;
    // todo add metadata property

    this.data = jest.fn(DocumentSnapshot.prototype._data);
    this.get = jest.fn(DocumentSnapshot.prototype._get);
    this.isEqual = jest.fn(DocumentSnapshot.prototype._isEqual);
  }

  _data() {
    FirestoreMock.checkThrow();
    return this.ref._data;
  }

  _get(fieldPath) {
    FirestoreMock.checkThrow();
    let result;

    if (fieldPath in this.ref._data) {
      result = this.ref._data[fieldPath]; // eslint-disable-line security/detect-object-injection
    }
    return result;
  }

  _isEqual(other) {
    FirestoreMock.checkThrow();
    return other && other instanceof DocumentSnapshot && other.id === this.id;
  }

  mockClear() {
    this.data.mockClear();
    this.get.mockClear();
    this.isEqual.mockClear();
  }
}

module.exports = DocumentSnapshot;
