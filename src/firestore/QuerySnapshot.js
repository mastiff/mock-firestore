/*
 * Copyright (c) 2018 Mastiff
 *
 * This file is licensed under the MIT license found in the LICENSE file in the
 * root directory of this source tree.
 *
 */

/* eslint-disable no-underscore-dangle */
const FirestoreMock = require('./FirestoreMock');
const QueryDocumentSnapshot = require('./QueryDocumentSnapshot');

// https://firebase.google.com/docs/reference/js/firebase.firestore.QuerySnapshot
class QuerySnapshot {
  constructor(query, docChanges) {
    this.query = query;
    this.docs = query._docs.map((doc) => new QueryDocumentSnapshot(doc));
    this.size = this.docs.length;
    this.empty = this.size === 0;
    // todo implement metadata property

    this._docChanges = docChanges;

    this.docChanges = jest.fn(QuerySnapshot.prototype._docChanges);
    this.forEach = jest.fn(QuerySnapshot.prototype._forEach);
    this.isEqual = jest.fn(QuerySnapshot.prototype._isEqual);
  }

  _docChanges(snapshotOptions) {
    FirestoreMock.checkThrow();
    return this._docChanges;
  }

  _forEach(callback, thisArg = this) {
    FirestoreMock.checkThrow();
    this.docs.forEach((doc, index, array) => callback.call(thisArg, doc, index, array));
  }

  _isEqual(other) {
    FirestoreMock.checkThrow();
    return other && other instanceof QuerySnapshot && other.query === this.query;
  }

  mockClear() {
    this.docChanges.mockClear();
    this.forEach.mockClear();
    this.isEqual.mockClear();
  }
}

module.exports = QuerySnapshot;
