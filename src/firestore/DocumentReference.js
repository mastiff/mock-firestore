/*
 * Copyright (c) 2018 Mastiff
 *
 * This file is licensed under the MIT license found in the LICENSE file in the
 * root directory of this source tree.
 *
 */

/* eslint-disable no-underscore-dangle */
const DocumentSnapshot = require('./DocumentSnapshot');
const FirestoreMock = require('./FirestoreMock');
const { genId } = require('../util');

// https://firebase.google.com/docs/reference/js/firebase.firestore.DocumentReference
class DocumentReference {
  constructor(parent, id = genId()) {
    // Implementation properties
    this.id = id;
    this.parent = parent;
    this.firestore = parent.firestore;

    // Mock properties
    this._path = `${parent._path}/${id}`;
    this._watchers = [];
    this._data = {};
    this._collections = [];
    this._exists = false;

    this.collection = jest.fn(DocumentReference.prototype._collection);
    this.delete = jest.fn(DocumentReference.prototype._delete);
    this.get = jest.fn(DocumentReference.prototype._get);
    this.onSnapshot = jest.fn(DocumentReference.prototype._onSnapshot);
    this.set = jest.fn(DocumentReference.prototype._set);
    this.update = jest.fn(DocumentReference.prototype._update);
  }

  _collection(collectionName) {
    let result = this._collections
      .filter((collection) => collection.id === collectionName)
      .reduce((acc, cv) => cv, null);

    if (!result) {
      result = new this.firestore._CollectionReference(this, collectionName);
      this._collections.push(result);
    }

    return result;
  }

  _delete() {
    return new Promise((resolve, reject) => {
      FirestoreMock.checkThrow();

      if (this._exists) {
        resolve(FirestoreMock.validateAction('delete', this));
      } else {
        reject(new Error('DocumentReference does not exist'));
      }
    }).then(() => {
      this._exists = false;
      this._notifyWatchers();
      return null;
    });
  }

  _get() {
    return FirestoreMock.checkThrowPromise()
      .then(() => FirestoreMock.validateAction('get', this))
      .then(() => new DocumentSnapshot(this));
  }

  _onSnapshot(fn) {
    const toCall = fn;
    const unsub = jest.fn(() => {
      this._watchers = this._watchers.filter((watcher) => watcher !== toCall);
    });
    this._watchers.push(toCall);
    toCall(new DocumentSnapshot(this));
    // setTimeout(() => toCall(new DocumentSnapshot(this)), 0);
    toCall.__unsubscribe = unsub;

    return unsub;
  }

  _set(data, options) {
    return FirestoreMock.checkThrowPromise()
      .then(() => {
        const cleanData = JSON.parse(JSON.stringify(data));
        const newData = (options && options.merge)
          ? Object.assign({}, this._data, cleanData)
          : cleanData;

        return Promise.all([
          newData,
          FirestoreMock.validateAction(this._exists ? 'update' : 'create', this, newData)
        ]);
      })
      .then(([newData]) => {
        this._exists = true;
        this._data = newData;
        this._notifyWatchers();
        return null;
      });
  }

  _update(data) {
    return FirestoreMock.checkThrowPromise()
      .then(() => {
        if (!this._exists) {
          throw new Error('DocumentReference does not exist');
        }
        const newData = Object.assign({}, this._data, JSON.parse(JSON.stringify(data)));

        return Promise.all([
          newData,
          FirestoreMock.validateAction('update', this, newData)
        ]);
      })
      .then(([newData]) => {
        this._data = newData;
        this._notifyWatchers();
        return null;
      });
  }

  _notifyWatchers() {
    this._watchers.forEach((watcher) => watcher(new DocumentSnapshot(this)));
  }

  mockClear() {
    this.collection.mockClear();
    this.delete.mockClear();
    this.get.mockClear();
    this.onSnapshot.mockClear();
    this.set.mockClear();
    this.update.mockClear();
  }
}

module.exports = DocumentReference;
