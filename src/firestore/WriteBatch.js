/*
 * Copyright (c) 2018 Mastiff
 *
 * This file is licensed under the MIT license found in the LICENSE file in the
 * root directory of this source tree.
 *
 */
const FirestoreMock = require('./FirestoreMock');

// https://firebase.google.com/docs/reference/js/firebase.firestore.WriteBatch
class WriteBatch {
  constructor() {
    FirestoreMock.checkThrow();
    this.toSet = [];
    this.toUpdate = [];
    this.toDelete = [];
    const that = this;

    this.set = jest.fn((ref, data) => {
      FirestoreMock.checkThrow();
      that.toSet.push({ ref, data });
      return that;
    });
    this.update = jest.fn((ref, data) => {
      FirestoreMock.checkThrow();
      that.toUpdate.push({ ref, data });
      return that;
    });
    this.delete = jest.fn((ref) => {
      FirestoreMock.checkThrow();
      that.toDelete.push({ ref });
      return that;
    });
    this.commit = jest.fn(() => FirestoreMock.checkThrowPromise()
      .then(() => {
        // todo "rollback" if commit fails?
        const commitPromises = [];
        that.toSet.forEach((el) => commitPromises.push(el.ref.set(el.data)));
        that.toUpdate.forEach((el) => commitPromises.push(el.ref.update(el.data)));
        that.toDelete.forEach((el) => commitPromises.push(el.ref.delete()));
        return Promise.all(commitPromises);
      }));
  }
}

module.exports = WriteBatch;
